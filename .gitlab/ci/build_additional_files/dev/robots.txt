########## robots.txt #######################################################################################
#
#   File for bots.
#   If you are reading this file,
#   you can display humans.txt file.
#
#############################################################################################################

# Allowing "Aasqatasun" crawler access to all content
# see: https://asqatasun.org
User-Agent: asqatasun
Allow: /
Disallow:

# Allowing "W3C-checklink" crawler access to all content
# see: https://dev.w3.org/perl/modules/W3C/LinkChecker/docs/checklink#bot
User-Agent: W3C-checklink
Allow: /
Disallow:

#############################################################################################################

# Allowing all web crawlers access to all content
    # User-agent: *
    # Disallow:

# Blocking all web crawlers from all content
User-agent: *
Disallow: /

#############################################################################################################
